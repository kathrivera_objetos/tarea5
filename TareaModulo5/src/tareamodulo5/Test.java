package tareamodulo5;

import helpers.Circulo;
import helpers.Cuadrado;
import helpers.Linea;
import helpers.Triangulo;

public class Test {
    public static void main(String[] args) {
      Circulo c_circulo = new Circulo();
      Triangulo c_triangulo = new Triangulo();
      Linea c_linea = new Linea();
      Cuadrado c_cuadrado = new Cuadrado();
      
      //info circulo
     System.out.print("Figura: ");
     c_circulo.Dibujar();
     System.out.println("\nColor: "+c_circulo.getColor()+"\nCalculo: "+c_circulo.calcularRadio(25));
     
           //info Triangulo
     System.out.print("\nFigura: ");
     c_triangulo.Dibujar();
     System.out.println("\nColor: "+c_triangulo.getColor()+"\nCalculo: "+c_triangulo.calcularTrianguloArea(12,25));
      
            //info Linea
     System.out.print("\nFigura: ");
     c_linea.Dibujar();
     System.out.println("\nColor: "+c_linea.getColor()+"\nCalculo: -----");
     
           //info Cuadrado
     System.out.print("\nFigura: ");
     c_cuadrado.Dibujar();
     System.out.println("\nColor: "+c_cuadrado.getColor()+"\nCalculo: "+c_cuadrado.calcularCuadradoArea(25));
      
    }
}
