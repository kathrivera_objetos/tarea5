package helpers;
public class Cuadrado extends Formas{
    public Cuadrado(){
        setColor("Blanco");
    }
    public void Dibujar(){
        System.out.print(dibujar("Cuadrado"));
    }
    public double calcularCuadradoArea(double lado){
        return (lado*lado);
    }
}
