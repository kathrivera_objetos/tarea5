package helpers;
public class Triangulo extends Formas{
    public Triangulo(){
        setColor("Negro");
    }
    public void Dibujar(){
        System.out.print(dibujar("Triangulo"));
    }
    public double calcularTrianguloArea(double base, double altura){
        return (base*altura)/2;
    }
}
