package helpers;
public class Circulo extends Formas{
    public Circulo(){
        setColor("Rojo");
    }
    public void Dibujar(){
        System.out.print(dibujar("Circulo"));
    }
    public double calcularRadio(double area){
        return Math.sqrt(area/Math.PI);
    }
}
